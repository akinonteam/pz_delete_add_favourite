import { observe, store } from 'shop-packages';
import { setBasket } from 'shop-packages/redux/basket/reducer';
import {ApiClient} from 'shop-packages';
import { basketListSelector } from 'shop-packages/redux/basket/selectors';

export default class DeleteAddToFavourites{
  pk;

  constructor() {
    this.selectors = {
      basketList: '.js-basket-list',
      basketModal: '.js-basket-modal',
      basketModalClose: '.js-basket-modal-close',
      basketModalCancel: '.js-basket-modal-cancel',
      basketOverlay: '.js-overlay',
      basketItemDeleteAddFavourite: '.js-delete-add-favourite',
      basketItemDelete: '.js-basket-item-delete',
      basketItemProductName: '.js-basket-modal-product-name',
      buttonRemove: '.js-basket-item-remove-button',
      basketProductName: '.js-basket-item-product-name',
      basketItem: '.js-basket-item',
      basketPkSelector: '.js-basket-item-remove-button',
    };

    this.basketModal = document.querySelector(this.selectors.basketModal);
    this.basketItemDeleteToAddFavouriteBtn = document.querySelector(this.selectors.basketItemDeleteAddFavourite);
    this.basketItemDeleteBtn = document.querySelector(this.selectors.basketItemDelete);

    observe(basketListSelector).subscribe(({ pending }) => {
      if (!pending) {
        this.bindEvents();
      }
    });
  }

  bindEvents = () => {
    document.querySelectorAll(this.selectors.buttonRemove).forEach((button) => {
      button.addEventListener('click', this.onRemoveButtonClick, true);
    });

    this.basketItemDeleteBtn.addEventListener('click', this.onBasketItemDeleteBtnClick, true);

    if (this.basketItemDeleteToAddFavouriteBtn) {
      this.basketItemDeleteToAddFavouriteBtn.addEventListener('click', this.onBasketItemDeleteToAddFavouriteBtn, true);
    }

    this.basketModal.querySelector(this.selectors.basketModalClose).addEventListener('click', this.closeModal, true);
    this.basketModal.querySelector(this.selectors.basketOverlay).addEventListener('click', this.closeModal, true);
    this.basketModal.querySelector(this.selectors.basketModalCancel).addEventListener('click', this.closeModal, true);
  }

  showModal = () => {
    this.basketModal.removeAttribute('hidden');
  }

  closeModal = () => {
    this.basketModal.setAttribute('hidden', 'hidden');
  }

  onRemoveButtonClick = (e) => {
    const target = e.target;
    const basketItem = target.closest(this.selectors.basketItem);
    const pkElement = target.closest(this.selectors.basketPkSelector);

    e.preventDefault();
    e.stopImmediatePropagation();
    this.showModal();

    if (target) {
      const productName = basketItem.querySelector(this.selectors.basketProductName);
      this.pk = parseInt($(pkElement).data('pk'));
      document.querySelector(this.selectors.basketItemProductName).innerHTML = productName.innerText;
    }
    return false;
  }

  onBasketItemDeleteBtnClick = async () => {
    const btnText = this.basketItemDeleteBtn.innerText;

    this.basketItemDeleteBtn.setAttribute('disabled', 'disabled');
    this.basketItemDeleteBtn.classList.add('loading');
    this.basketItemDeleteBtn.innerText = '';

    const { data } = await ApiClient.basket.removeProduct(this.pk);
    this.basketItemDeleteBtn.removeAttribute('disabled');

    this.basketModal.setAttribute('hidden', 'hidden');
    this.basketItemDeleteBtn.classList.remove('loading');
    this.basketItemDeleteBtn.innerText = btnText;
    store.dispatch(setBasket(data));
  }

  onBasketItemDeleteToAddFavouriteBtn = async () => {
    if (!window.GLOBALS.userLoggedIn) {
      return;
    }

    if (this.pk) {
      const btnText = this.basketItemDeleteToAddFavouriteBtn.innerText;

      this.basketItemDeleteToAddFavouriteBtn.setAttribute('disabled', 'disabled');
      this.basketItemDeleteToAddFavouriteBtn.classList.add('loading');
      this.basketItemDeleteToAddFavouriteBtn.innerText = '';

      const { data } = await ApiClient.basket.removeProduct(this.pk);
      const inFavourite = await this.fetchFavouriteStatus(this.pk);

      if (!inFavourite) {
        await ApiClient.favorites.addFavourite(this.pk);
      }

      this.basketItemDeleteToAddFavouriteBtn.removeAttribute('disabled');
      this.basketItemDeleteToAddFavouriteBtn.classList.remove('loading');
      this.basketItemDeleteToAddFavouriteBtn.innerText = btnText;

      this.basketModal.setAttribute('hidden', 'hidden');
      store.dispatch(setBasket(data));
    }
  }

  fetchFavouriteStatus = (pk) => {
    const inFavouriteItem = ApiClient.favorites.fetchFavourites().then((response) => {
      if (response.data.results && response.data.results.length) {
        const results = response.data.results;
        const item = results.find((item2) => item2.product.pk === pk);

        return item;
      }
    });
    return inFavouriteItem;
  }
}