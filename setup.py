import setuptools

def readme():
    with open("README.md", "r") as f:
        return f.read()

setuptools.setup(
    name="pz_delete_add_favourite",
    version="1.0.0",
    author="Akinon",
    author_email="info@akinon.com",
    description="",
    long_description=readme(),
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    zip_safe=False,
    install_requires=['click', 'Jinja2'],
    classifiers=[
        "Programming Language :: Python :: 3",
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Framework :: Django",
    ],
    include_package_data=True
)