# pz_delete_add_favourite

With this package, a modal will be opened so that the deleted products on the cart page can be added to favorites.

### Usage
### 1. Install the app
At project root, create a requirements.txt if it doesn't exist and add the following line to it;

```
-e git+https://git@bitbucket.org/akinonteam/pz_delete_add_favourite.git@478923a#egg=pz_delete_add_favourite
```

Note: always make sure to use the latest commit id in the above example.
For more information about this syntax, check [pip install docs](https://pip.pypa.io/en/stable/reference/pip_install/#git).

Next, run the following command to install the package.

```bash
# in venv
pip install -r requirements.txt
```

### 2. Install the npm package

```bash
# in /templates
yarn add git+ssh://git@bitbucket.org:akinonteam/pz_delete_add_favourite.git#478923a
```

Make sure to use the same git commit id as in `requirements.txt`.

### 3. Add to the project

```python
# omnife_base/settings.py:

INSTALLED_APPS.append('pz_delete_add_favourite')
```

### 4. Import template:

```jinja
{# in templates/basket/index.html #}
include at the top
{% from 'pz_delete_add_favourite/index.html' import DeleteAddFavouriteModal with context %}

{# Insert the last line inside the "block content" tag on the page. #}
{{ DeleteAddFavouriteModal(icon_class_name="icon-cart") }}
```

### 5. Import and initialize JS

```js
// in templates/basket/index.js
import DeleteAddToFavourites from 'pz-delete-add-favourite';

// add to the last line in the constructor()
new DeleteAddToFavourites();

// templates/webpack/webpack.settings.js 
// find this code "&& !/node_modules\/shop-packages/.test(modulePath)" and add the following code in the bottom line
&& !/node_modules\/pz-*/.test(modulePath)

// Sample code
babelLoaderConfig: {
    exclude: (modulePath) => {
        return /node_modules/.test(modulePath)
        && !/node_modules\/shop-packages/.test(modulePath)
        && !/node_modules\/pz-*/.test(modulePath)
        // Swiper has ** operator and won't work on some legacy browsers
        && !/node_modules\/swiper/.test(modulePath);
    }
},
```

### 6. Import styles:

```scss
@import '~pz-delete-add-favourite';
```

## Customizing the Html (Jinja Macro)
```jinja
{# All parameters #}
class="" // Modal class name
icon_class_name="" // Modal icon class name
title="" // Modal title
description="" // Modal description
btn_cancel="" // Modal cancel button text
btn_delete="" // Modal delete button text
btn_delete_add_favourites="" // Modal delete add favourite button text

{{ DeleteAddFavouriteModal(
    class="", 
    icon_class_name="",
    title="",
    description="",
    btn_cancel="",
    btn_delete="",
    btn_delete_add_favourites=""
) }}
```